package org.campus.uno;

import java.util.ArrayList;
import java.util.Collections;

public class UnoSpiel
{
	private ArrayList<Karte> ablageStapel = new ArrayList<Karte>();
	private ArrayList<Karte> kartenStapel = new ArrayList<Karte>();

	private ArrayList<Spieler> mitspieler = new ArrayList<Spieler>();

	public UnoSpiel()
	{
		for (int index = 0; index < 2; index++)
		{
			for (int zahlenwert = 0; zahlenwert < 10; zahlenwert++)
			{
				kartenStapel.add(new Karte(Farbe.rot, zahlenwert));
				kartenStapel.add(new Karte(Farbe.blau, zahlenwert));
				kartenStapel.add(new Karte(Farbe.gruen, zahlenwert));
				kartenStapel.add(new Karte(Farbe.gelb, zahlenwert));
			}
		}

		Collections.shuffle(kartenStapel); // Karten mischen

	}

	public Karte abheben()
	{
		// sind noch genug karten am stapel?
		return kartenStapel.remove(0);
	}

	public void mitSpielen(Spieler neuerSpieler)
	{
		mitspieler.add(neuerSpieler);
	}

	public void austeilen()
	{
		for (int zaehler = 0; zaehler < 7; zaehler++)
		{
			// Jeden Mitspieler einmal "besuchen"
			for (Spieler sp : mitspieler)
			{
				Karte karte = abheben();
				sp.aufnehmen(karte);
			}
		}
		
		Karte temp = abheben();
		ablageStapel.add(temp);
	}

}
