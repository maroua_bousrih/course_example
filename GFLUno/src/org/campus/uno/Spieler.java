package org.campus.uno;

import java.util.ArrayList;

public class Spieler
{
	private String name;

	private ArrayList<Karte> handkarten = new ArrayList<Karte>();

	public Spieler(String name)
	{
		this.name = name;
	}

	public void aufnehmen(Karte karte)
	{
		handkarten.add(karte);
	}

	public String toString()
	{
		return name;
	}

	public Karte passendeKarte(Karte vergleich)
	{

		for (Karte karte : handkarten)
		{
			if(karte.match(vergleich))
			{
				handkarten.remove(karte);
				return karte;
			}
		}
		
		return null;
	}

}
